package com.vivek.ibmmqmessages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbmMqMessagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbmMqMessagesApplication.class, args);
	}

}
