package com.vivek.ibmmqmessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableJms
public class MQController {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/send")
    public String send(@RequestBody String message) {
        try {
            jmsTemplate.convertAndSend("DEV.QUEUE.1", message);
            return "Message sent";
        } catch(JmsException e) {
            e.printStackTrace();
            return "Failed! Message not sent";
        }
    }

    @GetMapping("/receive")
    public String receive() {
        try {
            return jmsTemplate.receiveAndConvert("DEV.QUEUE.1").toString();
        } catch(JmsException e) {
            e.printStackTrace();
            return "Failed to receive messages";
        }
    }
}
